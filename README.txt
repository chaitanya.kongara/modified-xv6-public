Task 1:
    waitx:
        First of all we need to add the variables wtotaltime,etime,ctime,rtime to the proc structure.Create a system call(settimes) to update the waiting time, running time after every clock tick.
        Declare the system call in proc.c and call it from trap.c(just after incrementing ticks variable.).So, these gets updated after every increment in ticks variable.
        In settimes function, we will traverse through the entire process table and increment certain fields based on the state.If the state is sleeping then we increment iotime field of that process.If the state is running increment rtime.If the state is runnable increment wtime.
        We will implement a new system call waitx it will fill the values into the given 2 addresses with values of wtime and rtime respectively and return value of this syscall is pid corresponding to that particular wtime, rtime.
        To create an user command time make a file time.c (and add it to the makefile), which consists of a calling fork and executing the command using exec in the child process and in the parent process call the waitx syscall.On summarising, to check the working of waitx syscall call for a command like time echo "hi", the result will be output of echo "hi" and time taken to complete the given task.
    ps:
        We will implement this by creating a new system call which prints the required things by traversing through the entire process table.Simply continue the loop in the case of unused state, in remaining cases we will simply print the fields pid,priority,state,r_time,w_time,n_run.
Task 2:
    FCFS(non-preemptive):
        We need to schedule the processes based on their creation time.Preferance should be given to process with least creation time and is in runnable state, it needs to be in non-preemptive mode so we should not call yield function in trap.c.
    PBS:
        Similar to FCFS compute with usage of yield and compare priority field of structure instead of ctime.
        Create a function call set_priority whose first argument is new_priority, 2nd argument is pid and if the new-priority is less than the previous one then we need to relinquish the cpu from currently running processes and re schedule.
    MLFQ:(I haven't implemented it )
Observations:
    when number processes created is 15(in benchmark.c)
        end and start time of FCFS: ctime = 320	etime = 2677 totaltime = 2357 ticks.
        end and start time of RR: ctime = 477	etime = 2936 totaltime = 2459 ticks.
        end and start time of PBS: ctime = 422	etime = 2734 totaltime = 2312 ticks.
        performance order: PBS > FCFS > RR
    when number processes created is 25(in benchmark.c)
        end and start time of FCFS: ctime = 316	etime = 6894
        totaltime = 6578 ticks.
        end and start time of RR: ctime = 990	etime = 7282
        totaltime = 6292 ticks.
        end and start time of PBS: ctime = 442	etime = 7035
        totaltime =  6593 ticks.
        performance order: PBS < FCFS < RR 


    